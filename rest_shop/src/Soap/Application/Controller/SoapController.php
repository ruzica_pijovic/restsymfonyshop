<?php

namespace App\Soap\Application\Controller;

use App\Core\Application\Service\Customer\GetCustomerOrdersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SoapController
 */
class SoapController extends AbstractController
{
    const WSDL = 'ClientOrders.wsdl';

    private $soapServer;

    /**
     * @Route("/soap")
     *
     * @param GetCustomerOrdersService $customerOrdersService
     *
     * @return Response
     */
    public function index(GetCustomerOrdersService $customerOrdersService)
    {
        $this->soapServer = new \SoapServer(self::WSDL, [
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
        ]);
        $this->soapServer->setObject($customerOrdersService);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $this->soapServer->handle();

        $response->setContent(ob_get_clean());

        return $response;
    }
}
