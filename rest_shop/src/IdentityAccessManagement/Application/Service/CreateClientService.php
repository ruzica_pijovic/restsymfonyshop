<?php

namespace App\IdentityAccessManagement\Application\Service;

use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class CreateClientService.
 */
class CreateClientService
{
    /**
     * @var ClientManagerInterface
     */
    private $clientManager;

    /**
     * CreateClientService constructor.
     * @param ClientManagerInterface $clientManager
     */
    public function __construct(ClientManagerInterface $clientManager)
    {
        $this->clientManager = $clientManager;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function execute(array  $data): array
    {
        if (empty($data['redirect-uri']) || empty($data['grant-type'])) {
            throw new BadRequestHttpException('Parameters missing.');
        }

        $clientManager = $this->clientManager;
        $client = $clientManager->createClient();
        $client->setRedirectUris([$data['redirect-uri']]);
        $client->setAllowedGrantTypes([$data['grant-type']]);
        $clientManager->updateClient($client);

        return [
            'client_id' => $client->getPublicId(),
            'client_secret' => $client->getSecret(),
        ];
    }
}
