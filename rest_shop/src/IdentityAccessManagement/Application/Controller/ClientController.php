<?php

namespace App\IdentityAccessManagement\Application\Controller;

use App\IdentityAccessManagement\Application\Service\CreateClientService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ClientController.
 */
class ClientController extends AbstractFOSRestController
{
    /**
     * @var CreateClientService
     */
    private $clientService;

    /**
     * ClientController constructor.
     *
     * @param CreateClientService $clientService
     */
    public function __construct(CreateClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * Create Client.
     *
     * @FOSRest\Post("/createClient")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function authenticationAction(Request $request)
    {
        try {
            $data = \json_decode($request->getContent(), true);

            return $this->handleView($this->view($this->clientService->execute($data)));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()]));
        }
    }
}
