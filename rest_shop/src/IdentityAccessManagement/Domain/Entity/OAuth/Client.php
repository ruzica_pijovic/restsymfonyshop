<?php

namespace App\IdentityAccessManagement\Domain\Entity\OAuth;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Client extends BaseClient
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $clientId
     *
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientId() : ?string
    {
        return $this->clientId;
    }
}
