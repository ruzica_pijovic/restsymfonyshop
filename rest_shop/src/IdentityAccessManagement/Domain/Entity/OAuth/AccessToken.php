<?php

namespace App\IdentityAccessManagement\Domain\Entity\OAuth;

use App\IdentityAccessManagement\Domain\Entity\User;
use FOS\OAuthServerBundle\Entity\AccessToken as BaseAccessToken;

/**
 * Class AccessToken
 */
class AccessToken extends BaseAccessToken
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var User
     */
    protected $user;
}
