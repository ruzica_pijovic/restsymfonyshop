<?php declare(strict_types=1);

namespace App\Core\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Order.
 *
 * @ORM\Entity
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Assert\PositiveOrZero()
     * @Assert\NotNull()
     */
    private $orderAmount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Assert\PositiveOrZero()
     */
    private $shippingAmount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Assert\PositiveOrZero()
     */
    private $taxAmount;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="orders")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @var Customer
     */
    private $customer;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getOrderAmount(): ?int
    {
        return $this->orderAmount;
    }

    /**
     * @param int $orderAmount
     */
    public function setOrderAmount(int $orderAmount): void
    {
        $this->orderAmount = $orderAmount;
    }

    /**
     * @return int|null
     */
    public function getShippingAmount(): ?int
    {
        return $this->shippingAmount;
    }

    /**
     * @param int $shippingAmount
     */
    public function setShippingAmount(?int $shippingAmount): void
    {
        $this->shippingAmount = $shippingAmount;
    }

    /**
     * @return int|null
     */
    public function getTaxAmount(): ?int
    {
        return $this->taxAmount;
    }

    /**
     * @param int $taxAmount
     */
    public function setTaxAmount(?int $taxAmount): void
    {
        $this->taxAmount = $taxAmount;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }
}
