<?php declare(strict_types=1);

namespace App\Core\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Country.
 *
 * @ORM\Entity
 * @ORM\Table(name="country")
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $alphaTwo;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $alphaThree;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getAlphaTwo(): ?string
    {
        return $this->alphaTwo;
    }

    /**
     * @param string|null $alphaTwo
     */
    public function setAlphaTwo(?string $alphaTwo): void
    {
        $this->alphaTwo = $alphaTwo;
    }

    /**
     * @return string|null
     */
    public function getAlphaThree(): ?string
    {
        return $this->alphaThree;
    }

    /**
     * @param string|null $alphaThree
     */
    public function setAlphaThree(?string $alphaThree): void
    {
        $this->alphaThree = $alphaThree;
    }
}
