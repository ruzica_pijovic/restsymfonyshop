<?php declare(strict_types=1);

namespace App\Core\Domain\Repository\Country;

use App\Core\Domain\Repository\BaseRepositoryInterface;

/**
 * Interface CountryRepositoryInterface.
 */
interface CountryRepositoryInterface extends BaseRepositoryInterface
{
}
