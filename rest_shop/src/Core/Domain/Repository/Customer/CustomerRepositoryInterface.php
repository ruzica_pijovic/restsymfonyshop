<?php declare(strict_types = 1);

namespace App\Core\Domain\Repository\Customer;

use App\Core\Domain\Entity\Customer;
use App\Core\Domain\Repository\BaseRepositoryInterface;

/**
 * Interface CustomerRepositoryInterface.
 */
interface CustomerRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param Customer $customer
     *
     * @return mixed
     */
    public function persist(Customer $customer);

    /**
     *
     */
    public function flush(): void;

    /**
     * @param Customer $customer
     */
    public function save(Customer $customer): void;

    /**
     * @param Customer $customer
     */
    public function delete(Customer $customer): void;
}
