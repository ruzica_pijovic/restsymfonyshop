<?php declare(strict_types = 1);

namespace App\Core\Domain\Repository\Order;

use App\Core\Domain\Entity\Order;
use App\Core\Domain\Repository\BaseRepositoryInterface;

/**
 * Interface OrderRepositoryInterface.
 */
interface OrderRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param Order $order
     *
     * @return mixed
     */
    public function persist(Order $order);

    /**
     *
     */
    public function flush(): void;

    /**
     * @param Order $order
     */
    public function save(Order $order): void;

    /**
     * @param Order $order
     */
    public function delete(Order $order): void;
}
