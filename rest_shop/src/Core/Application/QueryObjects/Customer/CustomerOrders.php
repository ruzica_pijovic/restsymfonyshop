<?php

namespace App\Core\Application\QueryObjects\Customer;

use App\Core\Domain\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CustomerOrders.
 */
class CustomerOrders
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CustomerOrders constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $customerId
     *
     * @return mixed
     */
    public function getOrdersForCustomer(int $customerId)
    {
        return $this->entityManager->createQueryBuilder()
            ->from(Order::class, 'o')
            ->select('o.id')
            ->addSelect('o.orderAmount')
            ->addSelect('o.shippingAmount')
            ->addSelect('o.taxAmount')
            ->where('o.customer = :customer')
            ->orderBy('o.orderAmount', 'DESC')
            ->setParameter('customer', $customerId)
            ->getQuery()->getArrayResult();
    }
}
