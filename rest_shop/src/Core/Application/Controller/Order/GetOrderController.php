<?php

namespace App\Core\Application\Controller\Order;

use App\Core\Application\Service\Order\GetOrderService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class GetOrderController.
 */
class GetOrderController extends AbstractFOSRestController
{
    /**
     * @var GetOrderService
     */
    private $getOrderService;

    /**
     * GetOrderController constructor.
     *
     * @param GetOrderService $getOrderService
     */
    public function __construct(GetOrderService $getOrderService)
    {
        $this->getOrderService = $getOrderService;
    }

    /**
     * Get single Orders.
     *
     * @Rest\Get("/order/{id}")
     *
     * @param int $id
     *
     * @return Response
     */
    public function getOrder(int $id): Response
    {
        try {
            return $this->handleView($this->view($this->getOrderService->execute($id)));
        } catch (EntityNotFoundException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()]));
        }
    }
}
