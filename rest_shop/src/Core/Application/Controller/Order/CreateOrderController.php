<?php declare(strict_types = 1);

namespace App\Core\Application\Controller\Order;

use App\Core\Application\Service\Order\CreateOrderService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class CreateOrderController.
 */
class CreateOrderController extends AbstractFOSRestController
{
    /**
     * @var CreateOrderService
     */
    private $createOrderService;

    /**
     * CreateOrderController constructor.
     *
     * @param CreateOrderService $createOrderService
     */
    public function __construct(CreateOrderService $createOrderService)
    {
        $this->createOrderService = $createOrderService;
    }

    /**
     * Create Order.
     *
     * @Rest\Post("/order")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createOrder(Request $request): Response
    {
        try {
            $order = $this->createOrderService->execute(is_string($request->getContent()) ? $request->getContent() : '');

            return $this->handleView($this->view($order));
        } catch (UnprocessableEntityHttpException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY));
        } catch (BadRequestHttpException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}
