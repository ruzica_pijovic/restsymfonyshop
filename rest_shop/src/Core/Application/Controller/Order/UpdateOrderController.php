<?php

namespace App\Core\Application\Controller\Order;

use App\Core\Application\Service\Order\UpdateOrderService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class UpdateOrderController.
 */
class UpdateOrderController extends AbstractFOSRestController
{
    /**
     * @var UpdateOrderService
     */
    private $updateOrderService;

    /**
     * UpdateOrderController constructor.
     *
     * @param UpdateOrderService $updateOrderService
     */
    public function __construct(UpdateOrderService $updateOrderService)
    {
        $this->updateOrderService = $updateOrderService;
    }

    /**
     * Update Order.
     *
     * @Rest\Put("/order/{orderId}")
     *
     * @param int     $orderId
     * @param Request $request
     *
     * @return Response
     */
    public function createOrder(int $orderId, Request $request): Response
    {
        try {
            $order = $this->updateOrderService->execute($orderId, is_string($request->getContent()) ? $request->getContent() : '');

            return $this->handleView($this->view($order));
        } catch (UnprocessableEntityHttpException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY));
        } catch (BadRequestHttpException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}
