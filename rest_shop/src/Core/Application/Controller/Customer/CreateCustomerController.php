<?php declare(strict_types = 1);

namespace App\Core\Application\Controller\Customer;

use App\Core\Application\Service\Customer\CreateCustomerService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class CreateCustomerController.
 */
class CreateCustomerController extends AbstractFOSRestController
{
    /**
     * @var CreateCustomerService
     */
    private $createCustomerService;

    /**
     * CreateCustomerController constructor.
     *
     * @param CreateCustomerService $createCustomerService
     */
    public function __construct(CreateCustomerService $createCustomerService)
    {
        $this->createCustomerService = $createCustomerService;
    }

    /**
     * Create Customer.
     *
     * @Rest\Post("/customer")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createOrder(Request $request): Response
    {
        try {
            $customer = $this->createCustomerService->execute(is_string($request->getContent()) ? $request->getContent() : '');

            return $this->handleView($this->view($customer));
        } catch (BadRequestHttpException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}
