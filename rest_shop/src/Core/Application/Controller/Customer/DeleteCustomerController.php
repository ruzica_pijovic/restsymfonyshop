<?php

namespace App\Core\Application\Controller\Customer;

use App\Core\Application\Service\Customer\DeleteCustomerService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DeleteCustomerController.
 */
class DeleteCustomerController extends AbstractFOSRestController
{
    /**
     * @var DeleteCustomerService
     */
    private $deleteCustomerService;


    /**
     * GetOrderController constructor.
     *
     * @param DeleteCustomerService $deleteCustomerService
     */
    public function __construct(DeleteCustomerService $deleteCustomerService)
    {
        $this->deleteCustomerService = $deleteCustomerService;
    }

    /**
     * Delete Customer.
     *
     * @Rest\Delete("/customer/{id}/delete")
     *
     * @param int $id
     *
     * @return Response
     */
    public function delete(int $id): Response
    {
        try {
            return $this->handleView($this->view($this->deleteCustomerService->execute($id)));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}
