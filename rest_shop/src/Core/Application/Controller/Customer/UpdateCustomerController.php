<?php

namespace App\Core\Application\Controller\Customer;

use App\Core\Application\Service\Customer\UpdateCustomerService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class UpdateCustomerController.
 */
class UpdateCustomerController extends AbstractFOSRestController
{
    /**
     * @var UpdateCustomerService
     */
    private $updateCustomerService;


    /**
     * UpdateCustomerController constructor.
     *
     * @param UpdateCustomerService $updateCustomerService
     */
    public function __construct(UpdateCustomerService $updateCustomerService)
    {
        $this->updateCustomerService = $updateCustomerService;
    }

    /**
     * Update Customer.
     *
     * @Rest\Put("/customer/{customerId}")
     *
     * @param int     $customerId
     * @param Request $request
     *
     * @return Response
     */
    public function createOrder(int $customerId, Request $request): Response
    {
        try {
            $order = $this->updateCustomerService->execute($customerId, is_string($request->getContent()) ? $request->getContent() : '');

            return $this->handleView($this->view($order));
        } catch (EntityNotFoundException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY));
        } catch (BadRequestHttpException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}
