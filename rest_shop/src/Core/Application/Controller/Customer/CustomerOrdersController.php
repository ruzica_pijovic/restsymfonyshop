<?php

namespace App\Core\Application\Controller\Customer;

use App\Core\Application\Service\Customer\GetCustomerOrdersService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class CustomerOrdersController.
 */
class CustomerOrdersController extends AbstractFOSRestController
{
    /**
     * @var GetCustomerOrdersService
     */
    private $getCustomerOrdersService;

    /**
     * GetOrderController constructor.
     *
     * @param GetCustomerOrdersService $getCustomerOrdersService
     */
    public function __construct(GetCustomerOrdersService $getCustomerOrdersService)
    {
        $this->getCustomerOrdersService = $getCustomerOrdersService;
    }

    /**
     * Get Customer orders.
     *
     * @Rest\Get("/customer/{id}/orders")
     *
     * @param int $id
     *
     * @return Response
     */
    public function getCustomerOrders(int $id): Response
    {
        try {
            return $this->handleView($this->view(['data' => []]));
        } catch (\Throwable $exception) {
            return $this->handleView(
                $this->view(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR)
            );
        }
    }
}
