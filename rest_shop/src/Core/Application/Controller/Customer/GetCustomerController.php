<?php

namespace App\Core\Application\Controller\Customer;

use App\Core\Application\Service\Customer\GetCustomerService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class GetCustomerController.
 */
class GetCustomerController extends AbstractFOSRestController
{
    /**
     * @var GetCustomerService
     */
    private $getCustomerService;

    /**
     * GetOrderController constructor.
     *
     * @param GetCustomerService $getCustomerService
     */
    public function __construct(GetCustomerService $getCustomerService)
    {
        $this->getCustomerService = $getCustomerService;
    }

    /**
     * Get single Customer.
     *
     * @Rest\Get("/customer/{id}")
     *
     * @param int $id
     *
     * @return Response
     */
    public function getOrder(int $id): Response
    {
        try {
            return $this->handleView($this->view($this->getCustomerService->execute($id)));
        } catch (EntityNotFoundException $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(['error' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}
