<?php

namespace App\Core\Application\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WelcomeController
 */
class WelcomeController extends AbstractController
{
    /**
     * @return Response
     */
    public function index()
    {
        return new JsonResponse(['data' => 'Welcome :)']);
    }
}
