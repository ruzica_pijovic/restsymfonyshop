<?php

namespace App\Core\Application\Service\Customer;

use App\Core\Application\Validator\Validator;
use App\Core\Domain\Entity\Customer;
use App\Core\Domain\Repository\Country\CountryRepositoryInterface;
use App\Core\Domain\Repository\Customer\CustomerRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class CreateCustomerService.
 */
class CreateCustomerService
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    /**
     * CreateCustomerService constructor.
     *
     * @param SerializerInterface         $serializer
     * @param Validator                   $validator
     * @param CustomerRepositoryInterface $customerRepository
     * @param CountryRepositoryInterface  $countryRepository
     */
    public function __construct(
        SerializerInterface $serializer,
        Validator $validator,
        CustomerRepositoryInterface $customerRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->serializer         = $serializer;
        $this->validator          = $validator;
        $this->customerRepository = $customerRepository;
        $this->countryRepository  = $countryRepository;
    }

    /**
     * @param string $data
     *
     * @return array
     *
     * @throws BadRequestHttpException
     */
    public function execute(string $data): array
    {
        $dataArray = \json_decode($data, true);

        if (!$dataArray) {
            throw new BadRequestHttpException('Bad request.');
        }

        $country = $dataArray['country'] ? $this->countryRepository->findById($dataArray['country']) : null;
        unset($dataArray['country']);

        /** @var Customer $customer */
        $customer = $this->serializer->deserialize(\json_encode($dataArray), Customer::class, 'json');
        $customer->setCountry($country);

        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($customer);

        if ($errors->count() > 0) {
            throw new BadRequestHttpException(json_encode($this->validator->parseErrors($errors)));
        }

        $this->customerRepository->save($customer);

        return [
            'status' => 'Success',
        ];
    }
}
