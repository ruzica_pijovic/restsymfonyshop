<?php

namespace App\Core\Application\Service\Customer;

use App\Core\Domain\Entity\Country;
use App\Core\Domain\Entity\Customer;
use App\Core\Domain\Repository\Customer\CustomerRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class GetCustomerService.
 */
class GetCustomerService
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;


    /**
     * GetOrderService constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param int $id
     *
     * @return array
     *
     * @throws EntityNotFoundException
     */
    public function execute(int $id): array
    {
        $customer = $this->customerRepository->findById($id);

        if (!$customer instanceof Customer) {
            throw new EntityNotFoundException('Customer does not exist.');
        }

        return [
            'id' => $customer->getId(),
            'firstName' => $customer->getFirstName(),
            'lastName' => $customer->getLastName(),
            'email' => $customer->getEmail(),
            'country' => $this->formatCountry($customer->getCountry()),
        ];
    }

    /**
     * @param Country|null $country
     *
     * @return array
     */
    private function formatCountry(?Country $country): ?array
    {
        if (!$country instanceof Country) {
            return null;
        }

        return [
            'id' => $country->getId(),
            'name' => $country->getName(),
            'alphaTwo' => $country->getAlphaTwo(),
            'alphaThree' => $country->getAlphaThree(),
        ];
    }
}
