<?php

namespace App\Core\Application\Service\Customer;

use App\Core\Application\Validator\Validator;
use App\Core\Domain\Entity\Customer;
use App\Core\Domain\Repository\Country\CountryRepositoryInterface;
use App\Core\Domain\Repository\Customer\CustomerRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class UpdateCustomerService.
 */
class UpdateCustomerService
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    /**
     * UpdateCustomerService constructor.
     *
     * @param SerializerInterface         $serializer
     * @param Validator                   $validator
     * @param CustomerRepositoryInterface $customerRepository
     * @param CountryRepositoryInterface  $countryRepository
     */
    public function __construct(
        SerializerInterface $serializer,
        Validator $validator,
        CustomerRepositoryInterface $customerRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->serializer         = $serializer;
        $this->validator          = $validator;
        $this->customerRepository = $customerRepository;
        $this->countryRepository  = $countryRepository;
    }

    /**
     * @param int    $customerId
     * @param string $data
     *
     * @return array
     *
     * @throws EntityNotFoundException
     * @throws BadRequestHttpException
     */
    public function execute(int $customerId, string $data): array
    {
        $customer = $this->customerRepository->findById($customerId);

        if (!$customer instanceof Customer) {
            throw new EntityNotFoundException('Customer does not exist.');
        }
        $dataArray = \json_decode($data, true);

        if (!$dataArray) {
            throw new BadRequestHttpException('Bad request.');
        }

        $country = $dataArray['country'] ? $this->countryRepository->findById($dataArray['country']) : $customer->getCountry();
        unset($dataArray['country']);

        /** @var Customer $customer */
        $customer = $this->serializer->deserialize(\json_encode($dataArray), Customer::class, 'json', ['object_to_populate' => $customer]);
        $customer->setCountry($country);

        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($customer);

        if ($errors->count() > 0) {
            return [
                'error' => $this->validator->parseErrors($errors),
            ];
        }

        $this->customerRepository->save($customer);

        return [
            'status' => 'Success',
        ];
    }
}
