<?php

namespace App\Core\Application\Service\Customer;

use App\Core\Application\QueryObjects\Customer\CustomerOrders;
use SoapVar;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class GetCustomerOrdersService.
 */
class GetCustomerOrdersService
{
    /**
     * @var CustomerOrders
     */
    private $customerOrders;


    /**
     * GetCustomerOrdersService constructor.
     *
     * @param CustomerOrders $customerOrders
     */
    public function __construct(CustomerOrders $customerOrders)
    {
        $this->customerOrders = $customerOrders;
    }

    /**
     * @param int $customerId
     *
     * @return string
     */
    public function getcustomerorders(int $customerId)
    {
        $orders = $this->customerOrders->getOrdersForCustomer($customerId);

        $return_array = new \ArrayObject();

        foreach ($orders as $order) {
            $webservice = new SoapVar($order, SOAP_ENC_OBJECT, null, null, 'return');

            $return_array->append($webservice);
        }

        return new SoapVar($return_array, SOAP_ENC_ARRAY, null, null, 'return');
    }
}
