<?php

namespace App\Core\Application\Service\Customer;

use App\Core\Domain\Entity\Customer;
use App\Core\Domain\Repository\Customer\CustomerRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class DeleteCustomerService.
 */
class DeleteCustomerService
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;


    /**
     * GetOrderService constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param int $id
     *
     * @return array
     *
     * @throws EntityNotFoundException
     */
    public function execute(int $id): array
    {
        $customer = $this->customerRepository->findById($id);

        if (!$customer instanceof Customer) {
            throw new EntityNotFoundException('Customer does not exist.');
        }

        $this->customerRepository->delete($customer);

        return ['data' => 'Success'];
    }
}
