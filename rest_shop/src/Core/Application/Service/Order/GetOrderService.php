<?php

namespace App\Core\Application\Service\Order;

use App\Core\Domain\Entity\Order;
use App\Core\Domain\Repository\Order\OrderRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class GetOrderService.
 */
class GetOrderService
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * GetOrderService constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param int $id
     *
     * @return array
     *
     * @throws EntityNotFoundException
     */
    public function execute(int $id): array
    {
        $order = $this->orderRepository->findById($id);

        if (!$order instanceof Order) {
            throw new EntityNotFoundException('Order does not exist.');
        }

        return [
            'id' => $order->getId(),
            'orderAmount' => $order->getOrderAmount(),
            'shippingAmount' => $order->getShippingAmount(),
            'taxAmount' => $order->getTaxAmount(),
            'customer_id' => $order->getCustomer()->getId(),
        ];
    }
}
