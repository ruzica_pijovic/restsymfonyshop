<?php

namespace App\Core\Application\Service\Order;

use App\Core\Application\Validator\Validator;
use App\Core\Domain\Entity\Customer;
use App\Core\Domain\Entity\Order;
use App\Core\Domain\Repository\Customer\CustomerRepositoryInterface;
use App\Core\Domain\Repository\Order\OrderRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class UpdateOrderService.
 */
class UpdateOrderService
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * GetOrderService constructor.
     *
     * @param OrderRepositoryInterface    $orderRepository
     * @param SerializerInterface         $serializer
     * @param Validator                   $validator
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SerializerInterface $serializer,
        Validator $validator,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param int    $orderId
     * @param string $data
     *
     * @return array
     *
     * @throws EntityNotFoundException
     */
    public function execute(int $orderId, string $data): array
    {
        $order = $this->orderRepository->findById($orderId);

        if (!$order instanceof Order) {
            throw new EntityNotFoundException('Order does not exist.');
        }

        $dataArray = \json_decode($data, true);

        if (!$dataArray) {
            throw new BadRequestHttpException('Bad request.');
        }

        $customer = isset($dataArray['customer']) ? $this->customerRepository->findById($dataArray['customer']) : $order->getCustomer();

        if (!$customer instanceof Customer) {
            throw new EntityNotFoundException('Customer does not exist.');
        }

        unset($dataArray['customer']);

        /** @var Order $order */
        $order = $this->serializer->deserialize(\json_encode($dataArray), Order::class, 'json', ['object_to_populate' => $order]);
        $order->setCustomer($customer);

        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($order);

        if ($errors->count() > 0) {
            throw new BadRequestHttpException(\json_encode($this->validator->parseErrors($errors)));
        }

        $this->orderRepository->save($order);

        return [
            'status' => 'Success',
        ];
    }
}
