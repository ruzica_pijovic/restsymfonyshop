<?php declare(strict_types = 1);

namespace App\Core\Application\Validator;

use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\TraceableValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Validator
 */
class Validator extends TraceableValidator
{
    /**
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        parent::__construct($validator);
    }

    /**
     * @param ConstraintViolationList $errors
     *
     * @return array
     */
    public function parseErrors(ConstraintViolationList $errors): array
    {
        $errorsArray = [];

        foreach ($errors->getIterator() as $val) {
            $key = $val->getPropertyPath();
            $errorsArray[$key] = $val->getMessage();
        }

        return $errorsArray;
    }

    /**
     * @param ConstraintViolationList $errors
     *
     * @return string
     */
    public function getFirstError(ConstraintViolationList $errors): string
    {
        /** @var ConstraintViolationList $errors */
        foreach ($errors->getIterator() as $val) {
            return $val->getMessage();
        }

        return '';
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function validateAndGetFirstError($value, $constraints = null, $groups = null): array
    {
        /** @var ConstraintViolationList $errors */
        $errors = $this->validate($value, $constraints, $groups);

        if ($errors->count() > 0) {
            return ['errors' => $this->getFirstError($errors)];
        }

        return [];
    }
}
