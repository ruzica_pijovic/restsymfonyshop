<?php

namespace App\Core\Infrastructure\Persistence\Doctrine\Repository\Country;

use App\Core\Domain\Entity\Country;
use App\Core\Domain\Repository\Country\CountryRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CountryRepository.
 */
class CountryRepository implements CountryRepositoryInterface
{
    private $repository;

    private $entityManager;

    /**
     * OrderRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Country::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     *
     * @return Country|object|null
     */
    public function findById(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * Find all method.
     *
     * @return object[]
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return object[]
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }
}
