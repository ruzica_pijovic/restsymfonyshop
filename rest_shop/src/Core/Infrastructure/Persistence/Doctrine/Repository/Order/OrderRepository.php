<?php

namespace App\Core\Infrastructure\Persistence\Doctrine\Repository\Order;

use App\Core\Domain\Entity\Order;
use App\Core\Domain\Repository\Order\OrderRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class OrderRepository.
 */
class OrderRepository implements OrderRepositoryInterface
{
    private $repository;

    private $entityManager;

    /**
     * OrderRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Order::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     *
     * @return Order|object|null
     */
    public function findById(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * Find all method.
     *
     * @return object[]
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return object[]
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param Order $order
     */
    public function persist(Order $order): void
    {
        $this->entityManager->persist($order);
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }

    /**
     * @param Order $order
     */
    public function save(Order $order): void
    {
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    /**
     * @param Order $order
     */
    public function delete(Order $order): void
    {
        $this->entityManager->remove($order);
        $this->entityManager->flush();
    }
}
