<?php

namespace App\Core\Infrastructure\Persistence\Doctrine\Repository\Customer;

use App\Core\Domain\Entity\Customer;
use App\Core\Domain\Repository\Customer\CustomerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CustomerRepository.
 */
class CustomerRepository implements CustomerRepositoryInterface
{
    private $repository;

    private $entityManager;

    /**
     * OrderRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Customer::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     *
     * @return Customer|object|null
     */
    public function findById(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * Find all method.
     *
     * @return object[]
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return object[]
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param Customer $customer
     */
    public function persist(Customer $customer): void
    {
        $this->entityManager->persist($customer);
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }

    /**
     * @param Customer $customer
     */
    public function save(Customer $customer): void
    {
        $this->entityManager->persist($customer);
        $this->entityManager->flush();
    }

    /**
     * @param Customer $customer
     */
    public function delete(Customer $customer): void
    {
        $this->entityManager->remove($customer);
        $this->entityManager->flush();
    }
}
