rest-shop.local
============

A Simple Symfony Rest project.

Some things mey not be working if you dont have https on your local machine and in couple of places you can find hardcoded url `https://rest-shop.local/`, please change that. 

---
Setup:

* docker-compose build
* docker-compose up -d
* `docker exec -it $(docker ps -aqf "name=shop-php") /bin/sh;` and create user with symfony command
---

APIs can be tested using postman collection export.
Be aware that export is not full overview of all available APIs. :(

---

Path to soap https://rest-shop.local/soap?wsdl
Use some soap client gor testing. Request should be POST on https://rest-shop.local/soap
---